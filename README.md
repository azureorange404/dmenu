# AzureOrange's patched dmenu

Patches applied to dmenu:
- dmenu-xyw
- dmenu-center
- dmenu-border
- dmenu-alpha

Selfmade patches:
- new flag -no-escape (disables escape key when set)

dmenu - dynamic menu
====================
dmenu is an efficient dynamic menu for X.


Requirements
------------
In order to build dmenu you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dmenu is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dmenu
(if necessary as root):

    make clean install


Running dmenu
-------------
See the man page for details.
